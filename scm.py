from trytond.model import ModelView, ModelSQL, fields, Model, Unique
from trytond.pyson import Eval, Id, Not
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool, PoolMeta
import hgapi
from git import Repo as GitRepo
import ConfigParser
import StringIO
import io
import os
import re


__all__ = [
    'SCMDeployment',
    'SCMDeploymentModule',
    'Party',
    'SCMRepository',
    'SCMModule',
    'SCMDeploymentExport',
    'SCMDeploymentExportStart',
    'SCMDeploymentExportResult',
    'SCMModuleImport',
    'SCMModuleImportStart',
    'SCMModuleImportResult']

__metaclass__ = PoolMeta

REPO_TYPE = [('hg', 'Mercurial'),
             ('git', 'Git')]
DEFAULT_BRANCH = {
    'hg': 'default',
    'git': 'master'}
STATES = {
    'readonly': ~Eval('active', True)}


class TrytonCFGFile(ConfigParser.ConfigParser):

    def export(self):
        output = StringIO.StringIO()
        for section in self.sections():
            output.write('[%s]\n' % section)
            for item in self.items(section):
                output.write('%s = %s\n' % (item[0], item[1]))
            output.write('\n')
        return output.getvalue()


class SCMDeployment(ModelSQL, ModelView):
    """SCM Deployment"""
    __name__ = 'scm.deployment'

    name = fields.Char('Name', required=True,
                       states=STATES, depends=['active'])
    version = fields.Char('Version', required=True,
                          states=STATES, depends=['active'])
    party = fields.Many2One('party.party', 'Party',
                            states=STATES, depends=['active'])
    modules = fields.Many2Many('scm.deployment-scm.module', 'deployment', 'module',
                               'Modules', states=STATES, depends=['active'])
    active = fields.Boolean('Active', select=True)

    @classmethod
    def __setup__(cls):
        super(SCMDeployment, cls).__setup__()
        cls._error_messages.update({
            'missing_dependencies': 'Missing dependencies: %s.'})

    @classmethod
    def default_active(cls):
        return True

    def export_configuration(self):
        config = TrytonCFGFile()
        total_modules = [m for m in self.modules]
        for module in self.modules:
            total_modules.extend(
                module.get_dependencies(
                    self.version, [
                        m.name for m in total_modules]))
        total_modules = sorted(set(total_modules))
        for module in total_modules:
            config.add_section(module.name)
            config.set(module.name, 'branch', self.version)
            config.set(module.name, 'repo', module.type)
            config.set(module.name, 'url', module.url)
            config.set(module.name, 'path', module.local_path)
            if module.main_branch != 'default':
                config.set(module.name, 'main_branch', module.repository._get_main_branch())
            if module.repository.local_path != module.local_path:
                config.set(module.name, 'repo_path',
                           '%s/%s' % (module.repository.local_path, module.repository.name))

        return unicode(config.export()).encode('utf-8')


class SCMDeploymentModule(ModelSQL):
    """SCM Deployment - Module"""
    __name__ = 'scm.deployment-scm.module'
    _table = 'scm_deployment_module_rel'

    deployment = fields.Many2One('scm.deployment', 'Deployment',
                                 ondelete='CASCADE', required=True,
                                 select=True)
    module = fields.Many2One('scm.module', 'Module',
                             ondelete='RESTRICT',
                             required=True, select=True)


class Party:
    __name__ = 'party.party'

    scm_repo_type = fields.Selection(
        REPO_TYPE, 'Repository type',
        states={'readonly': ~Eval('active'),
                'invisible': Not(Id('scm', 'category_tryton_dev').in_(
                                      Eval('categories', [])))},
        depends=['active'])
    scm_url = fields.Char('Repository URL',
                          states={'readonly': ~Eval('active'),
                                  'invisible': Not(Id('scm', 'category_tryton_dev').in_(
                                      Eval('categories', [])))},
                          depends=['active'])
    scm_path = fields.Char('Local Path',
                           states={'readonly': ~Eval('active'),
                                   'invisible': Not(Id('scm', 'category_tryton_dev').in_(
                                       Eval('categories', [])))},
                           depends=['active'])

    @classmethod
    def default_scm_repo_type(cls):
        return 'hg'


class SCMRepository(ModelSQL, ModelView):
    """SCM Repository"""
    __name__ = 'scm.repository'

    name = fields.Char('Name', required=True,
                       states=STATES, depends=['active'])
    type = fields.Selection(REPO_TYPE,
                            'Repository type', required=True,
                            states={'readonly': ~Eval('active', True)},
                            depends=['active'])
    url = fields.Char('URL', required=True,
                      states={'readonly': ~Eval('active', True)},
                      depends=['active'])
    local_path = fields.Char('Local Path', required=True,
                             states={'readonly': ~Eval('active', True) | Eval('modules', [])},
                             depends=['active', 'modules'])
    owner = fields.Many2One('party.party', 'Owner',
                            ondelete='RESTRICT',
                            domain=[('categories', '=', Id('scm', 'category_tryton_dev'))],
                            states={'readonly': ~Eval('active', True)},
                            depends=['active'])
    active = fields.Boolean('Active')
    main_branch = fields.Selection([('default', 'default'),
                                    ('stable', 'stable')], 'Main branch',
                                   translate=False,
                                   states={'readonly': ~Eval('active', True)},
                                   depends=['active'])
    modules = fields.One2Many('scm.module', 'repository', 'Modules')
    # todo: list of branches in repo (readonly function)

    @classmethod
    def __setup__(cls):
        super(SCMRepository, cls).__setup__()

    @classmethod
    def default_active(cls):
        return True

    @classmethod
    def default_type(cls):
        return 'hg'

    @classmethod
    def default_main_branch(cls):
        return 'default'

    @fields.depends('owner', 'type', 'url', 'path', 'name')
    def on_change_owner(self, field=None):
        if not self.owner:
            return
        self.type = self.owner.scm_repo_type if self.owner.scm_repo_type else 'hg'
        if self.owner.scm_url:
            self.url = '%s%s' % (self.owner.scm_url, self.name)
        self.local_path = self.owner.scm_path or None

    @classmethod
    def create(cls, vlist):
        Configuration = Pool().get('scm.configuration')
        modules = super(SCMRepository, cls).create(vlist)
        if Configuration(1).clone_on_create:
            cls.clone(modules)
        return modules

    @classmethod
    def clone(cls, records):
        for record in records:
            record._clone()

    def _clone(self):
        if os.path.exists(self._get_path()) and os.listdir(self._get_path()):
            return
        getattr(self, '_clone_%s' % self.type)()

    def _clone_hg(self):
        extended_args = ['--pull']
        hgapi.hg_clone(self.url, self._get_path(), *extended_args)

    def _clone_git(self):
        GitRepo.clone_from(self.url, self._get_path())

    @classmethod
    def pull(cls, records):
        for record in records:
            record._pull()

    def _pull(self, version=None):
        getattr(self, '_pull_%s' % self.type)(version)

    def _pull_hg(self, version=None):
        repo = hgapi.Repo(self._get_path())
        repo.hg_pull()
        if version:
            revision = version
            if revision not in repo.get_branch_names():
                revision = self._get_main_branch()
            repo.hg_update(revision, clean=True)

    def _pull_git(self, version=None):
        repo = GitRepo(self._get_path()[:-1])
        o = repo.remotes.origin
        o.pull()
        revision = version
        if revision not in repo.heads:
            revision = self._get_main_branch()
        repo.heads[revision].checkout()

    def _get_main_branch(self):
        if self.main_branch == 'default':
            return DEFAULT_BRANCH[self.type]
        return self.main_branch

    def _get_path(self):
        Configuration = Pool().get('scm.configuration')
        configuration = Configuration(1)
        if not configuration.repo_path:
            configuration.raise_user_error('missing_repo_path')
        my_path = '%s/%s/%s' % (configuration.repo_path,
                                (self.local_path[:-1]
                                 if self.local_path.endswith(
                                    os.path.sep) else self.local_path),
                                self.name)
        if not my_path.endswith(os.path.sep):
            my_path += os.path.sep
        return my_path


class SCMModule(ModelSQL, ModelView):
    """SCM Module"""
    __name__ = 'scm.module'

    name = fields.Char('Name', required=True,
                       states=STATES, depends=['active'])
    local_path = fields.Char('Local Path', required=True,
                             states={'readonly': ~Eval('active', True)},
                             depends=['active'])
    active = fields.Boolean('Active')
    repository = fields.Many2One('scm.repository', 'Repository',
                                 select=True, required=True,
                                 ondelete='RESTRICT',
                                 states={'readonly': ~Eval('active', True)},
                                 depends=['active'])
    owner = fields.Function(
        fields.Many2One('party.party', 'Owner',
                        ondelete='RESTRICT',
                        domain=[('categories', '=', Id('scm', 'category_tryton_dev'))],
                        states={'readonly': ~Eval('active', True)},
                        depends=['active']),
        'get_repository_field', searcher='search_repository_field')
    # todo: list of dependencies (readonly function)

    @classmethod
    def __setup__(cls):
        super(SCMModule, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_uk1', Unique(t, t.name), 'Module name must be unique.')
        ]
        cls._error_messages.update({
            'missing_dependencies':
                ('Module "%s" has got some missing dependencies: "%s".'),
            'wrong_local_path': 'Local path of module "%s" must match with path of its repository.'})

    def __getattr__(self, name):
        try:
            return super(SCMModule, self).__getattr__(name)
        except AttributeError:
            pass
        return getattr(self.repository, name)

    @classmethod
    def validate(cls, records):
        super(SCMModule, cls).validate(records)
        for record in records:
            if not str(record.local_path).startswith('%s' % record.repository.local_path):
                cls.raise_user_error('wrong_local_path', record.name)

    @classmethod
    def default_active(cls):
        return True

    @fields.depends('repository')
    def on_change_repository(self, field=None):
        if not self.repository:
            return
        self.owner = self.repository.owner
        self.local_path = '%s' % self.repository.local_path

    def get_repository_field(self, name=None):
        if self.repository:
            value = getattr(self.repository, name, None)
            if isinstance(value, Model):
                return value.id
            return value
        return None

    @classmethod
    def search_repository_field(cls, name, clause):
        return [('repository.' + name, ) + tuple(clause[1:])]

    def _get_path(self):
        Configuration = Pool().get('scm.configuration')
        configuration = Configuration(1)
        if not configuration.repo_path:
            configuration.raise_user_error('missing_repo_path')
        my_path = '%s/%s/%s' % (configuration.repo_path,
                                (self.local_path[:-1]
                                 if self.local_path.endswith(
                                    os.path.sep) else self.local_path),
                                self.name)
        if not my_path.endswith(os.path.sep):
            my_path += os.path.sep
        return my_path

    def get_dependencies(self, version, to_discard=[]):
        pool = Pool()
        Module = pool.get('scm.module')

        if not os.path.exists(
                self._get_path()) or not os.listdir(self._get_path()):
            self.repository._clone()
        if not os.path.exists('%s/tryton.cfg' % self._get_path()[:-1]):
            return []
        module_names = [m.name for m in self.repository.modules if m.id != self.id]
        # Many modules in a repository: avoid to pull many times
        if not any(m in to_discard for m in module_names):
            self.repository._pull(version)
        config = ConfigParser.ConfigParser()
        config.readfp(open('%s/tryton.cfg' % self._get_path()[:-1]))
        info = dict(config.items('tryton'))
        for key in ('depends', 'extras_depend', 'xml'):
            if key in info:
                info[key] = info[key].strip().splitlines()
        requires = []
        for dep in info.get('depends', ''):
            if not re.match(r'(ir|res)(\W|$)', dep):
                requires.append(dep)

        requires = [item for item in requires if item not in to_discard]
        if not requires:
            return []
        modules = Module.search([('name', 'in', requires)])
        if not modules or len(modules) != len(requires):
            missing = [
                dep for dep in requires if dep not in [
                    m.name for m in modules]]
            self.raise_user_error('missing_dependencies', (self.name, missing))
        value = []
        value.extend(modules)
        to_discard += requires
        for module in modules:
            others = module.get_dependencies(version, to_discard)
            if others:
                value.extend(others)
        return value


class SCMDeploymentExportStart(ModelView):
    """Export SCM Deployment"""
    __name__ = 'scm.deployment.export.start'


class SCMDeploymentExportResult(ModelView):
    """Export SCM Deployment"""
    __name__ = 'scm.deployment.export.result'

    file = fields.Binary('File', readonly=True)


class SCMDeploymentExport(Wizard):
    """Export SCM Module"""
    __name__ = 'scm.deployment.export'

    start = StateTransition()
    result = StateView('scm.deployment.export.result',
                       'scm.deployment_export_view',
                       [Button('Close', 'end', 'tryton-close'), ]
                       )

    def transition_start(self):
        pool = Pool()
        Scm_deployment = pool.get('scm.deployment')
        scm_deployment = Scm_deployment(Transaction().context['active_id'])
        file_data = scm_deployment.export_configuration()
        self.result.file = buffer(file_data) if file_data else None
        return 'result'

    def default_result(self, fields):
        file_ = self.result.file
        self.result.file = False  # No need to store it in session
        return {
            'file': file_,
        }


class SCMModuleImportStart(ModelView):
    """Import SCM Module"""
    __name__ = 'scm.module.import.start'

    file = fields.Binary('File', readonly=False)


class SCMModuleImportResult(ModelView):
    """Import SCM Module"""
    __name__ = 'scm.module.import.result'


class SCMModuleImport(Wizard):
    """Import SCM Module"""
    __name__ = 'scm.module.import'

    start = StateView('scm.module.import.start',
                      'scm.module_import_view',
                      [Button('Cancel', 'end', 'tryton-cancel'),
                       Button('Import', 'result', 'tryton-ok', default=True)]
                      )

    result = StateTransition()

    def transition_result(self):
        pool = Pool()
        Scm_module = pool.get('scm.module')
        Scm_repo = pool.get('scm.repository')

        config = ConfigParser.ConfigParser()
        config.readfp(io.BytesIO(self.start.file))
        sections = config.sections()
        modules = []
        repositories = []
        repo_names = []
        for section in sections:
            _name = section
            _path = config.get(section, 'path')
            if config.has_option(section, 'repo_path'):
                _path = config.get(section, 'repo_path')
                _name = _path[_path.rfind('/') + 1:]
            if _name in repo_names:
                continue
            value = {'name': _name,
                     'type': config.get(section, 'repo'),
                     'url': config.get(section, 'url'),
                     'local_path': _path[:_path.rfind('/')]}
            if config.has_option(section, 'main_branch'):
                value['main_branch'] = config.get(section, 'main_branch')
            repositories.append(value)
            repo_names.append(_name)

        if repositories:
            repositories = Scm_repo.create(repositories)

        for section in sections:
            _name = section
            if config.has_option(section, 'repo_path'):
                _name = _path[_path.rfind('/') + 1:]
            _repo, = [r for r in repositories if r.name == _name]
            modules.append({
                'name': section,
                'repository': _repo.id,
                'local_path': config.get(section, 'path')
            })
        Scm_module.create(modules)
        return 'end'
